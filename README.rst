GCPy
====

This is a stub package designed as a mock-up for the new GEOS-Chem Python package and documentation. A live version can be found `here <http://danielrothenberg.com/gcpy/>`_.
